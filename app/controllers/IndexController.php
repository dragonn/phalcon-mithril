<?php

class IndexController extends ControllerBase {
    public function indexAction() {
        $this->assets->addCss('css/pure.css');
        $this->assets->addJs('js/mithril/index.build.js');
    }
}

