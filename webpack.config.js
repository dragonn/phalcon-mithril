const path = require('path');

module.exports = {
  entry: {
      login: './app/mithril/login/index.js',
      index: './app/mithril/index/index.js',
  },
  output: {
    path: path.resolve(__dirname, './public/js/mithril/'),
    filename: '[name].build.js'
  }
};